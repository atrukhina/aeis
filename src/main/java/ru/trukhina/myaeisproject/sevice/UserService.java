package ru.trukhina.myaeisproject.sevice;

import javax.ejb.Local;
import ru.trukhina.myaeisproject.model.User;

@Local
public interface UserService {
    
    User createUser(User user);
    
    User editUser(User user);
    
    void deleteUser(User user);
    
    User getUserByEmail(String email);
}
