package ru.trukhina.myaeisproject.dao;

import javax.ejb.Stateless;
import ru.trukhina.myaeisproject.model.User;

@Stateless
public class UserDao extends GenericDaoJpa<User, String>{

    public UserDao() {
        super(User.class);
    }
}
