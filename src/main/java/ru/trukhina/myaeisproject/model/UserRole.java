package ru.trukhina.myaeisproject.model;

public enum UserRole {
    ADMIN,
    USER
}
